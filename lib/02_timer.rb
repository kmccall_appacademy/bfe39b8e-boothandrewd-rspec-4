# lib/02_timer.rb

class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = seconds / 60**2
    seconds_after_hours = seconds % 60**2
    minutes = seconds_after_hours / 60
    seconds_after_minutes = seconds_after_hours % 60
    '%02d:%02d:%02d' % [hours, minutes, seconds_after_minutes]
  end
end
