class Temperature
  attr_reader :in_fahrenheit, :in_celsius

  def self.ftoc(f_temp)
    (f_temp - 32) * (5.0 / 9.0)
  end

  def self.ctof(c_temp)
    c_temp * (9.0 / 5.0) + 32.0
  end

  def self.from_fahrenheit(f_temp)
    new(f: f_temp)
  end

  def self.from_celsius(c_temp)
    new(c: c_temp)
  end

  def initialize(options)
    value = options.first[-1]
    if options.keys.first == :f
      @in_fahrenheit = value
      @in_celsius = self.class.ftoc(value)
    else
      @in_celsius = value
      @in_fahrenheit = self.class.ctof(value)
    end
  end
end

class Celsius < Temperature
  def initialize(c_temp)
    super(c: c_temp)
  end
end

class Fahrenheit < Temperature
  def initialize(f_temp)
    super(f: f_temp)
  end
end
