# lib/04_dictionary.rb

class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(addition)
    # byebug
    if addition.class == Hash
      @entries[addition.keys.first] = addition.first[-1]
    else
      @entries[addition] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    @entries.include?(word)
  end

  def find(search)
    @entries.reject do |key|
      (key =~ Regexp.new(search)).nil?
    end.keys.each_with_object({}) do |key, matches|
      matches[key] = @entries[key]
    end || {}
  end

  def printable
    keywords.reduce('') do |str, key|
      str + "[#{key}] \"#{@entries[key]}\"\n"
    end.chomp
  end
end
