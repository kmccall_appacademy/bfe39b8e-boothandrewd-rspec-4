# lib/01_book_titles.rb

class Book
  def initialize
    @little_words = %w[a the and in an of]
  end

  def title=(original_title)
    @original_title = original_title
  end

  def title
    # byebug
    @original_title.capitalize.split.map do |word|
      @little_words.include?(word) ? word : word.capitalize
    end.join ' '
  end
end
